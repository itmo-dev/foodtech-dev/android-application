package com.foodtechdev.app.JsonObjects.SurveyModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Editor {

    @SerializedName("editorId")
    @Expose
    private String editorId;
    @SerializedName("editorLogin")
    @Expose
    private String editorLogin;

    public Editor(String editorId, String editorLogin) {
        this.editorId = editorId;
        this.editorLogin = editorLogin;
    }

    public String getEditorId() {
        return editorId;
    }

    public String getEditorLogin() {
        return editorLogin;
    }

    @Override
    public String toString() {
        return "Editor{" +
                "editorId='" + editorId + '\'' +
                ", editorLogin='" + editorLogin + '\'' +
                '}';
    }
}

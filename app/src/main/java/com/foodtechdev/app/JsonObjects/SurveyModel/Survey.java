package com.foodtechdev.app.JsonObjects.SurveyModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Survey {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("questions")
    @Expose
    private List<Question> questions = null;

    public Survey(String id, Meta meta, List<Question> questions) {
        this.id = id;
        this.meta = meta;
        this.questions = questions;
    }

    @Override
    public String toString() {
        return "Survey{" +
                "id='" + id + '\'' +
                ", meta=" + meta +
                ", questions=" + questions +
                '}';
    }

    public String getId() {
        return id;
    }

    public Meta getMeta() {
        return meta;
    }

    public List<Question> getQuestions() {
        return questions;
    }

}

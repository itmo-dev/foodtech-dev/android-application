package com.foodtechdev.app.JsonObjects.SurveyModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Meta {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("company")
    @Expose
    private Company company;
    @SerializedName("editor")
    @Expose
    private Editor editor;
    @SerializedName("questionsNumber")
    @Expose
    private int questionsNumber;
    @SerializedName("confirmationMessage")
    @Expose
    private String confirmationMessage;

    public Meta(String title, String description, Company company, Editor editor, int questionsNumber, String confirmationMessage) {
        this.title = title;
        this.description = description;
        this.company = company;
        this.editor = editor;
        this.questionsNumber = questionsNumber;
        this.confirmationMessage = confirmationMessage;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "Meta{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", company=" + company +
                ", editor=" + editor +
                ", questionsNumber=" + questionsNumber +
                ", confirmationMessage='" + confirmationMessage + '\'' +
                '}';
    }

    public String getDescription() {
        return description;
    }

    public Company getCompany() {
        return company;
    }

    public Editor getEditor() {
        return editor;
    }

    public int getQuestionsNumber() {
        return questionsNumber;
    }

    public String getConfirmationMessage() {
        return confirmationMessage;
    }
}

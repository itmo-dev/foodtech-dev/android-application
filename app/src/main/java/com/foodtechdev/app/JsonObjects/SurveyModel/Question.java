package com.foodtechdev.app.JsonObjects.SurveyModel;

import java.util.List;

public class Question {
    private String id;
    private String title;
    private String type;
    private String helpText;
    private String placeholder;
    private List<String> options;
    private Boolean required;

    public Question(String type, String helpText, String placeholder, String title, String id, Boolean required, List<String> options) {
        this.type = type;
        this.helpText = helpText;
        this.placeholder = placeholder;
        this.title = title;
        this.id = id;
        this.required = required;
        this.options = options;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", type='" + type + '\'' +
                ", helpText='" + helpText + '\'' +
                ", placeholder='" + placeholder + '\'' +
                ", options=" + options +
                ", required=" + required +
                '}';
    }

    public String getType() {
        return type;
    }

    public String getHelpText() {
        return helpText;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    public Boolean getRequired() {
        return required;
    }

    public List<String> getOptions() {
        return options;
    }
}


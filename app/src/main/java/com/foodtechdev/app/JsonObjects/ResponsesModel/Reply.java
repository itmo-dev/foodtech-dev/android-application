package com.foodtechdev.app.JsonObjects.ResponsesModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Reply {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("questionId")
    @Expose
    private String questionId;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("selectedAnswer")
    @Expose
    private String selectedAnswer;
    @SerializedName("selectedAnswers")
    @Expose
    private List<String> selectedAnswers = null;

    public Reply(String type, String questionId, String text, String selectedAnswer, List<String> selectedAnswers) {
        this.type = type;
        this.questionId = questionId;
        this.text = text;
        this.selectedAnswer = selectedAnswer;
        this.selectedAnswers = selectedAnswers;
    }

    @Override
    public String toString() {
        return "Reply{" +
                "type='" + type + '\'' +
                ", questionId='" + questionId + '\'' +
                ", text='" + text + '\'' +
                ", selectedAnswer='" + selectedAnswer + '\'' +
                ", selectedAnswers=" + selectedAnswers +
                '}';
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getQuestionId() {
        return questionId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSelectedAnswer() {
        return selectedAnswer;
    }

    public void setSelectedAnswer(String selectedAnswer) {
        this.selectedAnswer = selectedAnswer;
    }

    public List<String> getSelectedAnswers() {
        return selectedAnswers;
    }

    public static class Builder {

        private Reply newReply;

        public Builder() {
            newReply = new Reply(null, null, null, null, null);
        }

        public Builder withType(String type) {
            newReply.type = type;
            return this;
        }

        public Builder withQuestionId(String questionId) {
            newReply.questionId = questionId;
            return this;
        }

        public Builder withText(String text) {
            newReply.text = text;
            return this;
        }

        public Builder withSelectedAnswer(String selectedAnswer) {
            newReply.selectedAnswer = selectedAnswer;
            return this;
        }

        public Builder withSelectedAnswers(List<String> selectedAnswers) {
            newReply.selectedAnswers = selectedAnswers;
            return this;
        }

        public Reply build() {
            return newReply;
        }
    }
}

package com.foodtechdev.app.JsonObjects.ResponsesModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Responses {

    @SerializedName("surveyId")
    @Expose
    private String surveyId;
    @SerializedName("checkId")
    @Expose
    private String checkId;
    @SerializedName("answeredQuestionsNumber")
    @Expose
    private int answeredQuestionsNumber;
    @SerializedName("replies")
    @Expose
    private List<Reply> replies = null;

    public Responses(String surveyId, int answeredQuestionsNumber, List<Reply> replies) {
        this.surveyId = surveyId;
        this.answeredQuestionsNumber = answeredQuestionsNumber;
        this.replies = replies;
    }

    @Override
    public String toString() {
        return "Responses{" +
                "surveyId='" + surveyId + '\'' +
                ", checkId='" + checkId + '\'' +
                ", answeredQuestionsNumber=" + answeredQuestionsNumber +
                ", replies=" + replies +
                '}';
    }

    public String getSurveyId() {
        return surveyId;
    }

    public String getCheckId() {
        return checkId;
    }

    public int getAnsweredQuestionsNumber() {
        return answeredQuestionsNumber;
    }

    public List<Reply> getReplies() {
        return replies;
    }


}

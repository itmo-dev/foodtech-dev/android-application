package com.foodtechdev.app;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.foodtechdev.app.ui.survey.EndFragment;
import com.foodtechdev.app.ui.survey.ErrorFragment;
import com.foodtechdev.app.ui.survey.LoadingFragment;
import com.foodtechdev.app.ui.survey.QuestionsFragment;
import com.foodtechdev.app.ui.survey.SurveyState;
import com.foodtechdev.app.ui.survey.SurveyViewModel;
import com.foodtechdev.app.ui.survey.WelcomeFragment;

public class SurveyActivity extends AppCompatActivity {
    private static final String TAG = SurveyActivity.class.getSimpleName();

    private SurveyViewModel surveyViewModel;

    private FrameLayout frameLayout;

    private boolean afterRestart = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);

        Log.d(TAG, "onCreate() called with: savedInstanceState = [" + savedInstanceState + "]");

        afterRestart = savedInstanceState != null;

        surveyViewModel = ViewModelProviders.of(this).get(SurveyViewModel.class);

        frameLayout = findViewById(R.id.survey_fragments_container);

        surveyViewModel.getSurveyStateLiveData().observe(this, this::onSurveyStateChanged);
    }

    private void onSurveyStateChanged(SurveyState surveyState) {
        Log.d(TAG, "onSurveyStateChanged() called with: surveyState = [" + surveyState + "]");

        if (afterRestart) {
            Log.d(TAG, "onSurveyStateChanged: not running because activity was restarted");
            afterRestart = false;
            return;
        }

        switch (surveyState) {
            case LOADING_SURVEY:
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.survey_fragments_container, new LoadingFragment())
                        .commit();
                loadSurvey();
                break;
            case WELCOME_SCREEN:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.survey_fragments_container, new WelcomeFragment())
                        .commit();
                break;
            case SURVEY_SCREEN:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.survey_fragments_container, new QuestionsFragment())
                        .commit();
                break;
            case SENDING_SURVEY:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.survey_fragments_container, new LoadingFragment())
                        .commit();
                break;
            case END_SCREEN:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.survey_fragments_container, new EndFragment())
                        .commit();
                break;
            case ERROR:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.survey_fragments_container, new ErrorFragment())
                        .commit();
                break;
        }
    }

    private void loadSurvey() {
        Log.d(TAG, "loadSurvey() called");

        String surveyId;

        final Intent intent = getIntent();
        final String action = intent.getAction();
        String data = intent.getDataString();

        if (!Intent.ACTION_VIEW.equals(action) || data == null) {
            data = getIntent().getStringExtra(MainActivity.EXTRA_SURVEY_LINK);
        }

        surveyId = data.substring(data.lastIndexOf("/") + 1);

        surveyViewModel.load(surveyId);
    }
}

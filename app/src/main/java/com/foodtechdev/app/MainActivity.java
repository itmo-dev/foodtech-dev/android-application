package com.foodtechdev.app;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_SURVEY_LINK = "com.foodtechdev.app.EXTRA_SURVEY_LINK";

    private EditText editTextSurveyLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextSurveyLink = findViewById(R.id.editText_surveyLink);
    }

    public void openSurvey(View view) {
        String id = editTextSurveyLink.getText().toString();
        if (id.isEmpty()) {
            Toast.makeText(this, "id is empty", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(this, SurveyActivity.class);
        intent.putExtra(EXTRA_SURVEY_LINK, id);
        startActivity(intent);
    }
}

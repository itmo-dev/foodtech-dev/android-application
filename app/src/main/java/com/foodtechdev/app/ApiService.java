package com.foodtechdev.app;

import com.foodtechdev.app.JsonObjects.ResponsesModel.Responses;
import com.foodtechdev.app.JsonObjects.SurveyModel.Survey;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiService {
    /*
    Retrofit get annotation with our URL
    And our method that will return us the List surveys
    */

    @GET("/surveys/{surveyId}")
    Call<Survey> getSurveyInfoJSON(@Path("surveyId") String id);

    @POST("/surveys/{surveyId}/responses")
    Call<Responses> postResponse(@Path("surveyId") String id, @Body Responses responses);
}

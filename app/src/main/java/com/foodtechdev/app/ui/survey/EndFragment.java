package com.foodtechdev.app.ui.survey;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.foodtechdev.app.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class EndFragment extends Fragment {

    private SurveyViewModel surveyViewModel;

    private TextView textViewMessage;
    private Button buttonEnd;

    public EndFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        surveyViewModel = ViewModelProviders.of(requireActivity()).get(SurveyViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_end, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        textViewMessage = view.findViewById(R.id.textView_confirmationMessage);
        buttonEnd = view.findViewById(R.id.button_endSurvey);

        textViewMessage.setText(surveyViewModel.getMeta().getConfirmationMessage());
        buttonEnd.setOnClickListener(v -> requireActivity().finish());
    }
}

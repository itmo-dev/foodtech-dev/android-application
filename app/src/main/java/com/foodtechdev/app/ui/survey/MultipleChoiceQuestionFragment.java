package com.foodtechdev.app.ui.survey;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.foodtechdev.app.JsonObjects.SurveyModel.Question;
import com.foodtechdev.app.R;

import java.util.List;

public class MultipleChoiceQuestionFragment extends Fragment {
    private static final String TAG = MultipleChoiceQuestionFragment.class.getSimpleName();

    private static final String ARG_QUESTION_POSITION = "param1";

    private SurveyViewModel surveyViewModel;

    private int questionPosition;

    private TextView textViewTitle;
    private TextView textViewRequired;
    private RadioGroup radioGroupAnswers;

    public MultipleChoiceQuestionFragment() {
        // Required empty public constructor
    }

    public static MultipleChoiceQuestionFragment newInstance(int questionPosition) {
        MultipleChoiceQuestionFragment fragment = new MultipleChoiceQuestionFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_QUESTION_POSITION, questionPosition);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        surveyViewModel = ViewModelProviders.of(requireActivity()).get(SurveyViewModel.class);

        if (getArguments() != null) {
            questionPosition = getArguments().getInt(ARG_QUESTION_POSITION);
        } else {
            Log.wtf(TAG, "onCreate: No arguments");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_multiple_choice_question, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Question question = surveyViewModel.getQuestion(questionPosition);
        textViewTitle = view.findViewById(R.id.textView_title);
        textViewTitle.setText(question.getTitle());

        textViewRequired = view.findViewById(R.id.textView_required);
        textViewRequired.setVisibility(question.getRequired() ? View.VISIBLE : View.GONE);

        radioGroupAnswers = view.findViewById(R.id.radioGroup_answers);
        radioGroupAnswers.removeAllViews();
        List<String> options = question.getOptions();
        for (int i = 0; i < options.size(); i++) {
            String option = options.get(i);
            RadioButton radioButton = new RadioButton(requireActivity());
            radioButton.setId(i);
            radioButton.setText(option);
            radioButton.setOnClickListener(v -> saveAnswer(((RadioButton) v).getText().toString()));
            radioGroupAnswers.addView(radioButton);
        }

        String selectedAnswer = surveyViewModel.getReplyForQuestion(questionPosition).getSelectedAnswer();
        if (selectedAnswer != null) {
            int id = question.getOptions().indexOf(selectedAnswer);
            radioGroupAnswers.check(id);
        }
    }

    private void saveAnswer(String answer) {
        surveyViewModel.setMultipleChoice(questionPosition, answer);
    }
}

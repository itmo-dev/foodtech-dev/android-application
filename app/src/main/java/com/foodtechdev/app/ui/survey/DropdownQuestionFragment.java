package com.foodtechdev.app.ui.survey;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.foodtechdev.app.JsonObjects.SurveyModel.Question;
import com.foodtechdev.app.R;

public class DropdownQuestionFragment extends Fragment {
    private static final String TAG = DropdownQuestionFragment.class.getSimpleName();

    private static final String ARG_QUESTION_POSITION = "param1";

    private SurveyViewModel surveyViewModel;

    private int questionPosition;

    private TextView textViewTitle;
    private TextView textViewRequired;
    private Spinner spinnerAnswers;

    public DropdownQuestionFragment() {
        // Required empty public constructor
    }

    public static DropdownQuestionFragment newInstance(int questionPosition) {
        DropdownQuestionFragment fragment = new DropdownQuestionFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_QUESTION_POSITION, questionPosition);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        surveyViewModel = ViewModelProviders.of(requireActivity()).get(SurveyViewModel.class);

        if (getArguments() != null) {
            questionPosition = getArguments().getInt(ARG_QUESTION_POSITION);
        } else {
            Log.wtf(TAG, "onCreate: No arguments");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dropdown_question, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Question question = surveyViewModel.getQuestion(questionPosition);
        textViewTitle = view.findViewById(R.id.textView_title);
        textViewTitle.setText(question.getTitle());

        textViewRequired = view.findViewById(R.id.textView_required);
        textViewRequired.setVisibility(question.getRequired() ? View.VISIBLE : View.GONE);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireActivity(), android.R.layout.simple_spinner_item, question.getOptions());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAnswers = view.findViewById(R.id.spinner_answers);
        spinnerAnswers.setAdapter(adapter);
        spinnerAnswers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "onItemSelected() called with: parent = [" + parent + "], view = [" + view + "], position = [" + position + "], id = [" + id + "]");

                String itemAtPosition = (String) parent.getItemAtPosition(position);
                saveAnswer(itemAtPosition);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        String selectedAnswer = surveyViewModel.getReplyForQuestion(questionPosition).getSelectedAnswer();
        if (selectedAnswer != null) {
            int id = question.getOptions().indexOf(selectedAnswer);
            spinnerAnswers.setSelection(id);
        }
    }

    private void saveAnswer(String answer) {
        surveyViewModel.setDropdown(questionPosition, answer);
    }
}

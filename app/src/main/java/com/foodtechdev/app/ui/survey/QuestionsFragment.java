package com.foodtechdev.app.ui.survey;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.foodtechdev.app.JsonObjects.SurveyModel.Question;
import com.foodtechdev.app.R;

public class QuestionsFragment extends Fragment {
    private static final String TAG = QuestionsFragment.class.getSimpleName();

    private SurveyViewModel surveyViewModel;

    private ViewPager viewPagerQuestions;
    private Button buttonNext;
    private Button buttonPrev;
    private TextView textViewQuestionNumber;
    private ProgressBar progressBarSurvey;

    private PagerAdapter pagerAdapter;

    public QuestionsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        surveyViewModel = ViewModelProviders.of(requireActivity()).get(SurveyViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_questions, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        viewPagerQuestions = view.findViewById(R.id.pager_question);
        buttonNext = view.findViewById(R.id.button_next);
        buttonPrev = view.findViewById(R.id.button_prev);
        textViewQuestionNumber = view.findViewById(R.id.textView_question_number);
        progressBarSurvey = view.findViewById(R.id.progressBar_survey);

        progressBarSurvey.setMax(surveyViewModel.getQuestionCount() - 1);

        textViewQuestionNumber.setText(getString(R.string.question_m_of_n, 1, surveyViewModel.getQuestionCount()));

        pagerAdapter = new QuestionPagerAdapter(requireFragmentManager());
        viewPagerQuestions.setAdapter(pagerAdapter);
        viewPagerQuestions.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                Log.d(TAG, "onPageSelected() called with: position = [" + position + "]");

                updateNextButtonText(position);

                progressBarSurvey.setProgress(position);
                textViewQuestionNumber.setText(getString(R.string.question_m_of_n, position + 1, surveyViewModel.getQuestionCount()));
            }
        });

        buttonNext.setOnClickListener(v -> {
            if (viewPagerQuestions.getCurrentItem() == surveyViewModel.getQuestionCount() - 1) {
                finishSurvey();
            } else {
                viewPagerQuestions.setCurrentItem(viewPagerQuestions.getCurrentItem() + 1);
            }
        });

        buttonPrev.setOnClickListener(v -> {
            if (viewPagerQuestions.getCurrentItem() == 0) {
                requireActivity().onBackPressed();
            } else {
                viewPagerQuestions.setCurrentItem(viewPagerQuestions.getCurrentItem() - 1);
            }
        });

        updateNextButtonText(0);

        surveyViewModel.getJumpToPageLiveData().observe(this, integer -> {
            viewPagerQuestions.setCurrentItem(integer);
        });
    }

    private void updateNextButtonText(int position) {
        if (position == surveyViewModel.getQuestionCount() - 1) {
            buttonNext.setText(getString(R.string.button_finish));
        } else {
            buttonNext.setText(getString(R.string.button_next));
        }
    }

    private void finishSurvey() {
        surveyViewModel.sendAndFinish();
    }

    private class QuestionPagerAdapter extends FragmentStatePagerAdapter {
        QuestionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Question question = surveyViewModel.getQuestion(position);
            switch (question.getType()) {
                case "TEXT":
                case "TEXTAREA":
                    return TextQuestionFragment.newInstance(position);
                case "MULTIPLE_CHOICE":
                    return MultipleChoiceQuestionFragment.newInstance(position);
                case "CHECKBOX":
                    return CheckboxQuestionFragment.newInstance(position);
                case "DROPDOWN":
                    return DropdownQuestionFragment.newInstance(position);
                default:
                    Log.d(TAG, "getItem: Not implemented fragment for type: " + question.getType());
                    Toast.makeText(getContext(), "Error: Not implemented fragment for type: " + question.getType(), Toast.LENGTH_SHORT).show();
                    return new Fragment();
            }
        }

        @Override
        public int getCount() {
            return surveyViewModel.getQuestionCount();
        }
    }
}

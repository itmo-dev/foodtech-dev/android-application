package com.foodtechdev.app.ui.survey;

public enum SurveyState {
    LOADING_SURVEY,
    WELCOME_SCREEN,
    SURVEY_SCREEN,
    END_SCREEN,
    SENDING_SURVEY,
    ERROR
}

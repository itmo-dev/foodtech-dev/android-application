package com.foodtechdev.app.ui.survey;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.foodtechdev.app.JsonObjects.ResponsesModel.Reply;
import com.foodtechdev.app.JsonObjects.ResponsesModel.Responses;
import com.foodtechdev.app.JsonObjects.SurveyModel.Meta;
import com.foodtechdev.app.JsonObjects.SurveyModel.Question;
import com.foodtechdev.app.JsonObjects.SurveyModel.Survey;
import com.foodtechdev.app.RetroClient;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SurveyViewModel extends AndroidViewModel {
    private static final String TAG = SurveyViewModel.class.getSimpleName();

    private MutableLiveData<SurveyState> surveyStateLiveData = new MutableLiveData<>();
    private MutableLiveData<Integer> jumpToPageLiveData = new MutableLiveData<>();
    private List<Reply> replyList = new ArrayList<>();
    private Survey survey;

    public SurveyViewModel(@NonNull Application application) {
        super(application);
        surveyStateLiveData.setValue(SurveyState.LOADING_SURVEY);
    }

    public MutableLiveData<SurveyState> getSurveyStateLiveData() {
        return surveyStateLiveData;
    }

    public MutableLiveData<Integer> getJumpToPageLiveData() {
        return jumpToPageLiveData;
    }

    public Reply getReplyForQuestion(int id) {
        return replyList.get(id);
    }

    public Meta getMeta() {
        return survey.getMeta();
    }

    public int getQuestionCount() {
        return survey.getQuestions().size();
    }

    public Question getQuestion(int id) {
        return survey.getQuestions().get(id);
    }

    public void load(String surveyId) {
        Log.d(TAG, "load() called with: surveyId = [" + surveyId + "]");

        Call<Survey> call = RetroClient.getApiService()
                .getSurveyInfoJSON(surveyId);

        call.enqueue(new Callback<Survey>() {
            @Override
            public void onResponse(Call<Survey> call, Response<Survey> response) {
                if (!response.isSuccessful()) {
                    Log.d(TAG, "onResponse: " + response.errorBody());

                    Toast.makeText(getApplication(), "Error: " + response.code(), Toast.LENGTH_LONG).show();

                    surveyStateLiveData.setValue(SurveyState.ERROR);
                    return;
                }

                survey = response.body();

                initResponse();

                surveyStateLiveData.setValue(SurveyState.WELCOME_SCREEN);
            }

            @Override
            public void onFailure(Call<Survey> call, Throwable t) {
                Log.d(TAG, "onFailure: Error!", t);

                Toast.makeText(getApplication(), "Error!", Toast.LENGTH_LONG).show();

                surveyStateLiveData.setValue(SurveyState.ERROR);
            }
        });
    }

    private void initResponse() {
        Log.d(TAG, "initResponse() called");

        int questionsNumber = getQuestionCount();

        if (!replyList.isEmpty()) {
            throw new RuntimeException("replyList is not empty!");
        }

        for (int i = 0; i < questionsNumber; i++) {
            replyList.add(new Reply.Builder()
                    .withQuestionId(survey.getId())
                    .withType(getQuestion(i).getType())
                    .withText("")
                    .withSelectedAnswer("")
                    .withSelectedAnswers(new ArrayList<>())
                    .build());
        }
    }

    public void begin() {
        Log.d(TAG, "begin() called");

        surveyStateLiveData.setValue(SurveyState.SURVEY_SCREEN);
    }

    public void setTextAnswer(int questionPosition, String selectedAnswer) {
        Log.d(TAG, "setTextAnswer() called with: questionPosition = [" + questionPosition + "], selectedAnswer = [" + selectedAnswer + "]");

        Reply reply = replyList.get(questionPosition);
        reply.setText(selectedAnswer);
    }

    public void setMultipleChoice(int questionPosition, String selectedAnswer) {
        Log.d(TAG, "setMultipleChoice() called with: questionPosition = [" + questionPosition + "], selectedAnswer = [" + selectedAnswer + "]");

        Reply reply = replyList.get(questionPosition);
        reply.setSelectedAnswer(selectedAnswer);
    }

    public void setCheckbox(int questionPosition, boolean checked, String selectedAnswer) {
        Log.d(TAG, "setCheckbox() called with: questionPosition = [" + questionPosition + "], checked = [" + checked + "], selectedAnswer = [" + selectedAnswer + "]");

        Reply reply = replyList.get(questionPosition);
        if (checked) {
            reply.getSelectedAnswers().add(selectedAnswer);
        } else {
            reply.getSelectedAnswers().remove(selectedAnswer);
        }
        Log.d(TAG, "setCheckbox: reply.getSelectedAnswers() = " + reply.getSelectedAnswers());
    }

    public void setDropdown(int questionPosition, String selectedAnswer) {
        Log.d(TAG, "setDropdown() called with: questionPosition = [" + questionPosition + "], selectedAnswer = [" + selectedAnswer + "]");

        Reply reply = replyList.get(questionPosition);
        reply.setSelectedAnswer(selectedAnswer);
    }

    public void sendAndFinish() {
        Log.d(TAG, "sendAndFinish() called");

        for (int i = 0; i < replyList.size(); i++) {
            if (getQuestion(i).getRequired()) {
                Reply reply = replyList.get(i);
                String type = reply.getType();
                if (((type.equals("TEXT") || type.equals("TEXTAREA")) && reply.getText().isEmpty())
                        || ((type.equals("DROPDOWN") || type.equals("MULTIPLE_CHOICE")) && reply.getSelectedAnswer().isEmpty())
                        || ((type.equals("CHECKBOX") && reply.getSelectedAnswers().isEmpty()))) {
                    jumpToPageLiveData.setValue(i);
                    return;
                }
            }
        }

        surveyStateLiveData.setValue(SurveyState.SENDING_SURVEY);

        List<Reply> replyListForSending = new ArrayList<>();
        for (int i = 0; i < replyList.size(); i++) {
            Reply reply = replyList.get(i);
            String type = reply.getType();
            if ((((type.equals("TEXT") || type.equals("TEXTAREA")) && !reply.getText().isEmpty())
                    || ((type.equals("DROPDOWN") || type.equals("MULTIPLE_CHOICE")) && !reply.getSelectedAnswer().isEmpty())
                    || ((type.equals("CHECKBOX") && !reply.getSelectedAnswers().isEmpty())))) {
                replyListForSending.add(reply);
            }
        }

        Log.d(TAG, "sendAndFinish: replyListForSending json: " + new Gson().toJson(replyListForSending));

        Responses responses = new Responses(survey.getId(), replyListForSending.size(), replyListForSending);

        Log.d(TAG, "sendAndFinish: responses json: " + new Gson().toJson(responses));

        Call<Responses> call = RetroClient.getApiService()
                .postResponse(survey.getId(), responses);

        call.enqueue(new Callback<Responses>() {
            @Override
            public void onResponse(Call<Responses> call, Response<Responses> response) {
                if (!response.isSuccessful()) {
                    Log.d(TAG, "onResponse: " + response.errorBody());

                    Toast.makeText(getApplication(), "Error: " + response.code(), Toast.LENGTH_LONG).show();

                    surveyStateLiveData.setValue(SurveyState.ERROR);
                    return;
                }

                surveyStateLiveData.setValue(SurveyState.END_SCREEN);
            }

            @Override
            public void onFailure(Call<Responses> call, Throwable t) {
                Log.d(TAG, "onFailure: Error!", t);

                Toast.makeText(getApplication(), "Error!", Toast.LENGTH_LONG).show();

                surveyStateLiveData.setValue(SurveyState.ERROR);
            }
        });
    }
}

package com.foodtechdev.app.ui.survey;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.foodtechdev.app.JsonObjects.SurveyModel.Question;
import com.foodtechdev.app.R;

public class TextQuestionFragment extends Fragment {
    private static final String TAG = TextQuestionFragment.class.getSimpleName();

    private static final String ARG_QUESTION_POSITION = "param1";

    private SurveyViewModel surveyViewModel;

    private int questionPosition;

    private TextView textViewTitle;
    private TextView textViewRequired;
    private EditText editTextAnswer;

    public TextQuestionFragment() {
        // Required empty public constructor
    }

    public static TextQuestionFragment newInstance(int questionPosition) {
        TextQuestionFragment fragment = new TextQuestionFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_QUESTION_POSITION, questionPosition);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        surveyViewModel = ViewModelProviders.of(requireActivity()).get(SurveyViewModel.class);

        if (getArguments() != null) {
            questionPosition = getArguments().getInt(ARG_QUESTION_POSITION);
        } else {
            Log.wtf(TAG, "onCreate: No arguments");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_text_question, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Question question = surveyViewModel.getQuestion(questionPosition);
        textViewTitle = view.findViewById(R.id.textView_title);
        textViewTitle.setText(question.getTitle());

        textViewRequired = view.findViewById(R.id.textView_required);
        textViewRequired.setVisibility(question.getRequired() ? View.VISIBLE : View.GONE);

        editTextAnswer = view.findViewById(R.id.editText_textQuestion_answer);
        editTextAnswer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                saveAnswer();
            }
        });

        String text = surveyViewModel.getReplyForQuestion(questionPosition).getText();
        if (text != null) {
            editTextAnswer.setText(text);
        }
    }

    private void saveAnswer() {
        String answer = editTextAnswer.getText().toString();
        surveyViewModel.setTextAnswer(questionPosition, answer);
    }
}

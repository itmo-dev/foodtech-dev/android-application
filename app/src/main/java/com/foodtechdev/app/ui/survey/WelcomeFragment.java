package com.foodtechdev.app.ui.survey;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.foodtechdev.app.JsonObjects.SurveyModel.Meta;
import com.foodtechdev.app.R;

public class WelcomeFragment extends Fragment {
    private static final String TAG = WelcomeFragment.class.getSimpleName();

    private SurveyViewModel surveyViewModel;

    private TextView textViewTitle;
    private TextView textViewDescription;
    private Button buttonStartSurvey;

    public WelcomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        surveyViewModel = ViewModelProviders.of(requireActivity()).get(SurveyViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_welcome, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        textViewTitle = view.findViewById(R.id.textView_surveyTitle);
        textViewDescription = view.findViewById(R.id.textView_surveyDescription);
        buttonStartSurvey = view.findViewById(R.id.button_startSurvey);

        Meta meta = surveyViewModel.getMeta();
        textViewTitle.setText(meta.getTitle());
        textViewDescription.setText(meta.getDescription());
        buttonStartSurvey.setOnClickListener(v -> surveyViewModel.begin());
    }
}

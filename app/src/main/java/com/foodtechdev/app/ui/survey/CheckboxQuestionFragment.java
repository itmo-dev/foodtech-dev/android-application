package com.foodtechdev.app.ui.survey;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.foodtechdev.app.JsonObjects.SurveyModel.Question;
import com.foodtechdev.app.R;

public class CheckboxQuestionFragment extends Fragment {
    private static final String TAG = CheckboxQuestionFragment.class.getSimpleName();

    private static final String ARG_QUESTION_POSITION = "param1";

    private SurveyViewModel surveyViewModel;

    private int questionPosition;

    private TextView textViewTitle;
    private TextView textViewRequired;
    private LinearLayout linearLayoutCheckBoxHolder;

    public CheckboxQuestionFragment() {
        // Required empty public constructor
    }

    public static CheckboxQuestionFragment newInstance(int questionPosition) {
        CheckboxQuestionFragment fragment = new CheckboxQuestionFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_QUESTION_POSITION, questionPosition);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        surveyViewModel = ViewModelProviders.of(requireActivity()).get(SurveyViewModel.class);

        if (getArguments() != null) {
            questionPosition = getArguments().getInt(ARG_QUESTION_POSITION);
        } else {
            Log.wtf(TAG, "onCreate: No arguments");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_checkbox_question, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Question question = surveyViewModel.getQuestion(questionPosition);
        textViewTitle = view.findViewById(R.id.textView_title);
        textViewTitle.setText(question.getTitle());

        textViewRequired = view.findViewById(R.id.textView_required);
        textViewRequired.setVisibility(question.getRequired() ? View.VISIBLE : View.GONE);

        linearLayoutCheckBoxHolder = view.findViewById(R.id.linearLayout_checkbox);
        linearLayoutCheckBoxHolder.removeAllViews();
        for (String option : question.getOptions()) {
            CheckBox checkBox = new CheckBox(requireActivity());
            checkBox.setText(option);
            checkBox.setOnClickListener(v -> saveAnswer(((CheckBox) v).isChecked(), ((CheckBox) v).getText().toString()));
            linearLayoutCheckBoxHolder.addView(checkBox);

            checkBox.setChecked(surveyViewModel.getReplyForQuestion(questionPosition).getSelectedAnswers().contains(option));
        }
    }

    private void saveAnswer(boolean checked, String answer) {
        surveyViewModel.setCheckbox(questionPosition, checked, answer);
    }
}
